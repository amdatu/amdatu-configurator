/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.configurationbuilder;

import java.util.function.Function;

/**
 * Specialized {@link ConfigurationValueReceiver} used when adapting another configuration
 *
 * @param <X> the property type of the property for which this receiver is accepting a value
 * @param <C> the configuration type for this configuration
 * @param <A> the configuration type for the configuration that's adapted
 */
public interface AdapterConfigurationValueReceiver<X, C, A> extends ConfigurationValueReceiver<X, C> {

	/**
	 *
	 * @param value
	 * @return
	 */
	AdapterConfiguration<C, A> value(X value);

	AdapterConfiguration<C, A> value(Function<A, X> valueFunction);

	AdapterConfiguration<C, A> value(Function<A, X> valueFunction, X defaultValue);
}
