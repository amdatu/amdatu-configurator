/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.osgi.framework.BundleContext;

/**
 * Test cases for {@link FrameworkPropertyReplacer}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class FrameworkPropertyReplacerTest {

    @Test
    public void testReplaceUsingFrameworkPropertyWithUnknownValuesOk() {
        BundleContext ctx = createMockContext();

        FrameworkPropertyReplacer replacer = new FrameworkPropertyReplacer(ctx, "", false);
        assertEquals("fwvalue", replacer.replace("fw_property"));
        assertEquals(null, replacer.replace("fw_unknown_property"));
    }

    @Test
    public void testReplaceUsingFrameworkPropertyWithoutUnknownValuesOk() {
        BundleContext ctx = createMockContext();

        FrameworkPropertyReplacer replacer = new FrameworkPropertyReplacer(ctx, "", true);
        assertEquals("fwvalue", replacer.replace("fw_property"));
        assertEquals("", replacer.replace("fw_unknown_property"));
    }

    @Test
    public void testReplaceUsingSystemPropertyWithUnknownValuesOk() {
        BundleContext ctx = createMockContext();

        FrameworkPropertyReplacer replacer = new FrameworkPropertyReplacer(ctx, "", false);
        assertEquals("sysvalue", replacer.replace("sys_property"));
        assertEquals(null, replacer.replace("sys_unknown_property"));
    }

    @Test
    public void testReplaceUsingSystemPropertyWithoutUnknownValuesOk() {
        BundleContext ctx = createMockContext();

        FrameworkPropertyReplacer replacer = new FrameworkPropertyReplacer(ctx, "", true);
        assertEquals("sysvalue", replacer.replace("sys_property"));
        assertEquals("", replacer.replace("sys_unknown_property"));
    }

    /**
     * AMDATUCONF-7 - Fall back to environment variables.
     */
    @Test
    public void testReplaceUsingEnvironmentPropertyOk() {
        BundleContext ctx = createMockContext();

        FrameworkPropertyReplacer replacer = new FrameworkPropertyReplacer(ctx, "", false);

        Map<String, String> env = System.getenv();
        if (!env.isEmpty()) {
            Entry<String, String> envEntry = env.entrySet().iterator().next();

            assertEquals(envEntry.getValue(), replacer.replace(envEntry.getKey()));
        }

        assertEquals(null, replacer.replace("no-such-environment-value-should-ever-exist"));
    }

    @Before
    public void setUp() {
        System.setProperty("sys_property", "sysvalue");
    }

    @After
    public void tearDown() {
        System.clearProperty("sys_property");
    }

    private BundleContext createMockContext() {
        BundleContext result = mock(BundleContext.class);
        when(result.getProperty(anyString())).then(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                String arg = (String) invocation.getArguments()[0];
                if ("fw_property".equals(arg)) {
                    return "fwvalue";
                }
                else if (arg.startsWith("sys")) {
                    return System.getProperty(arg);
                }
                return null;
            }
        });
        return result;
    }
}
