/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.itest;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

/**
 * Creates ad-hoc managed services when a bundle is started containing a certain
 * bundle header.
 */
public class TestBundleExtender implements BundleListener {
	public static final String KEY_MS_EXTENDER = "X-ManagedService";

	@Override
	public void bundleChanged(BundleEvent event) {
		int type = event.getType();
		if (type == BundleEvent.STARTED) {
			Bundle bundle = event.getBundle();

			Dictionary<String, String> headers = bundle.getHeaders();
			String val = (String) headers.get(KEY_MS_EXTENDER);
			if (val != null && !"".equals(val.trim())) {
				// register a custom managed service with the value as PID...
				BundleContext bc = bundle.getBundleContext();

				Dictionary<String, Object> props = new Hashtable<>();
				props.put(Constants.SERVICE_PID, val);

				bc.registerService(ManagedService.class.getName(), new ManagedService() {
					@Override
					public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
						System.out.println("Got " + properties + " for " + val);
					}
				}, props);
			}
		}
	}

}
