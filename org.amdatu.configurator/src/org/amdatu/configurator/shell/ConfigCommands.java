/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.shell;

import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_FILE_EXTENSIONS;
import static org.amdatu.configurator.shell.Utils.confirm;
import static org.amdatu.configurator.shell.Utils.getFileExtension;
import static org.amdatu.configurator.shell.Utils.printTable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.amdatu.configurator.Configurator;
import org.amdatu.configurator.shell.Utils.ConfigComparator;
import org.apache.felix.service.command.CommandSession;
import org.apache.felix.service.command.Descriptor;
import org.apache.felix.service.command.Parameter;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.log.LogService;

/**
 * Shell commands for Amdatu Configurator.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ConfigCommands {
	public static final String SCOPE = "config";
	public static final String[] FUNCTIONS = new String[] { "rm", "show", "update" };

	// Injected by Felix DM...
	private volatile BundleContext m_context;
	private volatile ConfigurationAdmin m_configAdmin;
	private volatile LogService m_log;

	@Descriptor("Removes an configuration matching the given PID.")
	public void rm(CommandSession session,
			@Descriptor("Asks for confirmation for each removal.") @Parameter(names = { "-i",
					"--interactive" }, presentValue = "true", absentValue = "false") boolean interactive,
			@Descriptor("Show verbose information about each removal.") @Parameter(names = { "-v",
					"--verbose" }, presentValue = "true", absentValue = "false") boolean verbose,
			@Descriptor("The PID of the configuration to remove, can be also a regular expression to match multiple configurations.") String pidRegex)
			throws Exception {
		Configuration[] configs = listConfigurations(null);
		if (configs != null) {
			List<Configuration> matches = new ArrayList<>(configs.length);

			Pattern pattern = Pattern.compile(pidRegex, Pattern.CASE_INSENSITIVE);
			for (Configuration config : configs) {
				if (matchesPid(config, pattern)) {
					matches.add(config);
				}
			}

			PrintStream console = session.getConsole();
			InputStream keyboard = session.getKeyboard();

			if (verbose) {
				if (matches.isEmpty()) {
					console.println("No configurations match...");
				} else {
					console.printf("Deleting %d configuration(s)...%n", matches.size());
				}
			}

			for (Configuration config : matches) {
				String pid = config.getPid();
				if (interactive) {
					if (!confirm(keyboard, console, "Delete configuration %s?", pid)) {
						continue;
					}
				}

				config.delete();

				if (verbose) {
					console.printf("Deleted configuration %s...%n", pid);
				}
				// Always log something to our main log...
				m_log.log(LogService.LOG_INFO, "Removed configuration " + pid + "...");
			}
		}
	}

	@Descriptor("Shows all current available configurations.")
	public void show(CommandSession session,
			@Descriptor("Only show the service (factory) PIDs, not the configuration parameters") @Parameter(names = {
					"-t", "--terse" }, presentValue = "true", absentValue = "false") boolean terse)
			throws Exception {
		show(session, terse, null);
	}

	@Descriptor("Shows all current available configurations matching the given filter.")
	public void show(CommandSession session,
			@Descriptor("Only show the service (factory) PIDs, not the configuration parameters") @Parameter(names = {
					"-t", "--terse" }, presentValue = "true", absentValue = "false") boolean terse,
			@Descriptor("The LDAP-style filter to apply.") String filter) throws Exception {
		Configuration[] configs = listConfigurations(filter);
		if (configs != null) {
			PrintStream console = session.getConsole();
			for (Configuration config : configs) {
				showConfig(console, config, terse);
			}
		}
	}

	@Descriptor("Updates one or more configurations from a given path.")
	public void update(CommandSession session,
			@Descriptor("Asks for confirmation for each update.") @Parameter(names = { "-i",
					"--interactive" }, presentValue = "true", absentValue = "false") boolean interactive,
			@Descriptor("The file or directory to update from") String path) throws Exception {
		PrintStream console = session.getConsole();
		InputStream keyboard = session.getKeyboard();

		File f = new File(path);
		if (f.isFile()) {
			if (interactive) {
				if (!confirm(keyboard, console, "Update configuration from \"%s\"?", f.getName())) {
					return;
				}
			}

			provisionFile(console, f);
		} else {
			for (File file : f.listFiles()) {
				if (interactive) {
					if (!confirm(keyboard, console, "Update configuration from \"%s\"?", file.getName())) {
						continue;
					}
				}

				provisionFile(console, file);
			}
		}
	}

	private Configuration[] listConfigurations(String filter) throws IOException, InvalidSyntaxException {
		// Assume empty strings are equivalent to no filter...
		if (filter != null && "".equals(filter.trim())) {
			filter = null;
		}

		Configuration[] configs = m_configAdmin.listConfigurations(filter);
		// AMDATUCONF-4: only sort when there's actually something to sort...
		if (configs != null) {
			Arrays.sort(configs, new ConfigComparator());
		}
		return configs;
	}

	private boolean matchesPid(Configuration config, Pattern pattern) {
		return pattern.matcher(config.getPid()).matches();
	}

	private void provisionFile(PrintStream console, File file) throws InvalidSyntaxException, IOException {
		String ext = getFileExtension(file);
		if (ext == null || "".equals(ext.trim())) {
			console.printf("Refusing to provision file without extension: \"%s\"!%n", file.getName());
			return;
		}

		boolean handled = false;
		// Try to find service that is willing to handle the given file...
		String filter = String.format("(%s=%s)", CONFIGURATOR_FILE_EXTENSIONS, ext);
		Collection<ServiceReference<Configurator>> serviceRefs = m_context.getServiceReferences(Configurator.class, filter);
		for (ServiceReference<Configurator> serviceRef : serviceRefs) {
			Configurator configurator = m_context.getService(serviceRef);
			if (configurator != null) {
				handled = configurator.provision(file);

				m_context.ungetService(serviceRef);

				if (handled) {
					break;
				}
			}
		}

		if (!handled) {
			if (serviceRefs == null || serviceRefs.size() < 1) {
				console.printf("Failed to provision file \"%s\": no configurator found!%n", file.getName());
			} else {
				console.printf("Failed to provision file \"%s\": no configurator was willing to handle it (see log for more information)!%n", file.getName());
			}
		}
	}

	private void showConfig(PrintStream console, Configuration config, boolean terse) {
		if (terse) {
			console.printf("%s", config.getPid());
		} else {
			printTable(console, config.getProperties());
		}
		console.println();
	}
}
