/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.shell;

import java.io.File;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Scanner;

import org.osgi.service.cm.Configuration;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
class Utils {
    private static final String SERVICE_BUNDLE_LOCATION = "service.bundleLocation";
    private static final String SERVICE_FACTORY_PID = "service.factoryPid";
    private static final String SERVICE_PID = "service.pid";
    
    public static String getFileExtension(File file) {
        if (file != null) {
            String name = file.getName();
            int idx = name.lastIndexOf('.');
            if (idx > 0) {
                return name.substring(idx, name.length());
            }
        }
        return null;
    }

    @SuppressWarnings("resource")
    public static boolean confirm(InputStream keyboard, PrintStream console, String prompt, Object... args) {
        Scanner scanner = new Scanner(keyboard).useDelimiter("[\r\n]");
        Boolean answer = null;
        do {
            console.printf(prompt, args);
            console.print("  [Y/n] ");

            String input = scanner.findWithinHorizon("(?i)\\b(y|yes|n|no)\\b", 0);
            if (input != null) {
                answer = isYes(input);
            }
        }
        while (answer == null);
        return answer.booleanValue();
    }

    /**
     * Prints a given dictionary of properties to a given {@link PrintStream}.
     * 
     * @param console the print stream to write to;
     * @param properties the properties to print as table.
     */
    public static void printTable(PrintStream console, Dictionary<String, Object> properties) {
        String[] keys = getOrderedKeys(properties);
        String[] values = getOrderedValues(properties, keys);

        int col1width = 1 + getMaxLength(keys);
        int col2width = 1 + getMaxLength(values);

        String col1line = repeatChar('-', 1 + col1width);
        String col2line = repeatChar('-', 1 + col2width);

        console.printf("+%s+%s+%n", col1line, col2line);
        console.printf("|%" + col1width + "s | %-" + col2width + "s|%n", "KEY", "VALUE");
        console.printf("+%s+%s+%n", col1line, col2line);
        for (int i = 0; i < keys.length; i++) {
            console.printf("|%" + col1width + "s | %-" + col2width + "s|%n", keys[i], values[i]);
        }
        console.printf("+%s+%s+%n", col1line, col2line);
    }

    private static int getMaxLength(String[] keys) {
        int result = 0;
        for (String key : keys) {
            result = Math.max(result, key.length());
        }
        return result;
    }

    private static String[] getOrderedKeys(Dictionary<String, Object> properties) {
        List<String> result = new ArrayList<>();
        Enumeration<String> keys = properties.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            if (SERVICE_FACTORY_PID.equals(key) || SERVICE_PID.equals(key) || SERVICE_BUNDLE_LOCATION.equals(key)) {
                continue;
            }
            result.add(key);
        }
        Collections.sort(result);
        if (properties.get(SERVICE_PID) != null) {
            result.add(0, SERVICE_PID);
        }
        if (properties.get(SERVICE_FACTORY_PID) != null) {
            result.add(0, SERVICE_FACTORY_PID);
        }
        if (properties.get(SERVICE_BUNDLE_LOCATION) != null) {
            result.add(0, SERVICE_BUNDLE_LOCATION);
        }
        return result.toArray(new String[result.size()]);
    }

    private static String[] getOrderedValues(Dictionary<String, Object> properties, String[] keys) {
        List<String> result = new ArrayList<>();
        for (String key : keys) {
            result.add(getValueAsString(properties.get(key)));
        }
        return result.toArray(new String[result.size()]);
    }

    private static String getValueAsString(Object value) {
        if (value == null) {
            return "<null>";
        }
        if (value.getClass().isArray()) {
            StringBuilder sb = new StringBuilder();
            sb.append('[');

            int len = Array.getLength(value);
            for (int i = 0; i < len; i++) {
                Object val = Array.get(value, i);
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(getValueAsString(val));
            }
            sb.append(']');

            return sb.toString();
        }
        return String.valueOf(value);
    }

    private static boolean isYes(String value) {
        value = value.toLowerCase();
        return "yes".equals(value) || "y".equals(value);
    }

    private static String repeatChar(char ch, int count) {
        return String.format("%0" + count + "d", 0).replace("0", "" + ch);
    }

    /**
     * Custom comparator that sorts {@link Configuration}s first by their factory PID, then by the service PID.
     */
    public static class ConfigComparator implements Comparator<Configuration> {
        @Override
        public int compare(Configuration o1, Configuration o2) {
            int result = 0;

            String fp1 = o1.getFactoryPid();
            String fp2 = o2.getFactoryPid();
            if (fp1 != null) {
                if (fp2 != null) {
                    result = fp1.compareTo(fp2);
                }
                else {
                    result = -1;
                }
            }
            else {
                if (fp2 != null) {
                    result = 1;
                }
            }

            if (result == 0) {
                String p1 = o1.getPid();
                String p2 = o2.getPid();

                result = p1.compareTo(p2);
            }

            return result;
        }
    }
}
