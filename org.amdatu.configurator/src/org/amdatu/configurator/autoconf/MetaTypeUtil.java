/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static org.amdatu.configurator.util.LoggerHelper.warn;
import static org.osgi.service.metatype.AttributeDefinition.BOOLEAN;
import static org.osgi.service.metatype.AttributeDefinition.BYTE;
import static org.osgi.service.metatype.AttributeDefinition.CHARACTER;
import static org.osgi.service.metatype.AttributeDefinition.DOUBLE;
import static org.osgi.service.metatype.AttributeDefinition.FLOAT;
import static org.osgi.service.metatype.AttributeDefinition.INTEGER;
import static org.osgi.service.metatype.AttributeDefinition.LONG;
import static org.osgi.service.metatype.AttributeDefinition.SHORT;
import static org.osgi.service.metatype.AttributeDefinition.STRING;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.felix.metatype.AD;
import org.apache.felix.metatype.Attribute;
import org.apache.felix.metatype.Designate;
import org.apache.felix.metatype.DesignateObject;
import org.apache.felix.metatype.MetaData;
import org.apache.felix.metatype.OCD;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class MetaTypeUtil {
    /**
     * Creates a new {@link MetaTypeUtil} instance.
     */
    private MetaTypeUtil() {
        // Nop
    }

    /**
     * Returns the content for the given attribute, or the defaults defines in the definition if no attribute is
     * specified, or no content is defined and the attribute definition is marked as optional.
     *
     * @param attribute the attribute to return the content for, can be <code>null</code>;
     * @param attributeDef the attribute definition to use, cannot be <code>null</code>.
     * @return the content, can be <code>null</code> if no attribute was given and no default was specified, or the
     *         attribute did not provide content.
     */
    public static String[] getContent(Attribute attribute, AD attributeDef) {
        String[] content = null;
        if (attribute != null) {
            content = attribute.getContent();
        }
        // Default values only make sense for optional attributes...
        if (content == null && !attributeDef.isRequired()) {
            content = attributeDef.getDefaultValue();
        }
        return content;
    }

    /**
     * Determines the value of an attribute based on an attribute definition
     *
     * @param attribute The attribute containing value(s)
     * @param ad The attribute definition
     * @return An <code>Object</code> reflecting what was specified in the attribute and it's definition.
     * @throws IllegalArgumentException in case the given content doesn't match the expected value, type or cardinality.
     */
    public static Object getValue(String[] content, int type, int cardinality) {
        if (content == null) {
            return null;
        }
        // verify correct type of the value(s)
        Object[] typedContent = null;
        try {
            for (int i = 0; i < content.length; i++) {
                String value = content[i];
                switch (type) {
                    case BOOLEAN:
                        if (typedContent == null) {
                            typedContent = new Boolean[content.length];
                        }
                        typedContent[i] = Boolean.valueOf(value);
                        break;
                    case BYTE:
                        if (typedContent == null) {
                            typedContent = new Byte[content.length];
                        }
                        typedContent[i] = Byte.valueOf(value);
                        break;
                    case CHARACTER:
                        if (typedContent == null) {
                            typedContent = new Character[content.length];
                        }
                        char[] charArray = value.toCharArray();
                        if (charArray.length == 1) {
                            typedContent[i] = Character.valueOf(charArray[0]);
                        }
                        else {
                            throw new IllegalArgumentException("Too many characters in value");
                        }
                        break;
                    case DOUBLE:
                        if (typedContent == null) {
                            typedContent = new Double[content.length];
                        }
                        typedContent[i] = Double.valueOf(value);
                        break;
                    case FLOAT:
                        if (typedContent == null) {
                            typedContent = new Float[content.length];
                        }
                        typedContent[i] = Float.valueOf(value);
                        break;
                    case INTEGER:
                        if (typedContent == null) {
                            typedContent = new Integer[content.length];
                        }
                        typedContent[i] = Integer.valueOf(value);
                        break;
                    case LONG:
                        if (typedContent == null) {
                            typedContent = new Long[content.length];
                        }
                        typedContent[i] = Long.valueOf(value);
                        break;
                    case SHORT:
                        if (typedContent == null) {
                            typedContent = new Short[content.length];
                        }
                        typedContent[i] = Short.valueOf(value);
                        break;
                    case STRING:
                        if (typedContent == null) {
                            typedContent = new String[content.length];
                        }
                        typedContent[i] = value;
                        break;
                    default:
                        // unsupported type
                        throw new IllegalArgumentException("Unsupported value-type");
                }
            }
        }
        catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("Invalid value-type");
        }

        // verify cardinality of value(s)
        Object result;
        if (cardinality == Integer.MIN_VALUE) {
            result = new Vector<>(asList(typedContent));
        }
        else if (cardinality == Integer.MAX_VALUE) {
            result = typedContent;
        }
        else if (cardinality < 0) {
            if (typedContent.length <= Math.abs(cardinality)) {
                result = new Vector<>(asList(typedContent));
            }
            else {
                throw new IllegalArgumentException("Expected at most " + Math.abs(cardinality) + " values");
            }
        }
        else if (cardinality > 0) {
            if (typedContent.length <= cardinality) {
                result = typedContent;
            }
            else {
                throw new IllegalArgumentException("Expected at most " + cardinality + " values");
            }
        }
        else /* cardinality == 0 */{
            if (typedContent.length == 1) {
                result = typedContent[0];
            }
            else {
                throw new IllegalArgumentException("Expected only a single value");
            }
        }
        return result;
    }

    /**
     * Extracts all designates from the given {@link MetaData} object and converts it to an {@link AutoConfResource}.
     *
     * @param origin the name of the file or resource the given {@link MetaData} originates from;
     * @param data the {@link MetaData} to convert.
     * @return a list of {@link AutoConfResource}s to provision, never <code>null</code>.
     */
    public static List<AutoConfResource> listAutoConfResources(String origin, MetaData data) {
		List<Designate> designates = data.getDesignates();
        Map<String, OCD> localOcds = data.getObjectClassDefinitions();
        if (localOcds == null) {
            localOcds = emptyMap();
        }

        List<AutoConfResource> result = new ArrayList<>();
        if (designates != null) {
            for (Designate designate : designates) {
                OCD ocd = null;

                DesignateObject object = designate.getObject();
                if (object != null) {
                    ocd = localOcds.get(object.getOcdRef());
                }

                result.add(new AutoConfResource(origin, designate, ocd));
            }
        }
        else {
            warn("No designates defined, plain MetaType file given? Source = \"%s\".", data.getSource());
        }
        return result;
    }
}
