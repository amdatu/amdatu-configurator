/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_RESOURCE_BUNDLE_LOCATION;
import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_RESOURCE_ORIGIN;
import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_RESOURCE_TYPE;
import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_TYPE_AUTOCONF;
import static org.amdatu.configurator.util.LoggerHelper.error;

import java.util.Arrays;
import java.util.Dictionary;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationPlugin;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

/**
 * Provides additional checks for AutoConf resources when they are provisioned to the actual {@link ManagedService} or
 * {@link ManagedServiceFactory}.
 */
public class AutoConfConfigurationChecker implements ConfigurationPlugin {
    private static final String DP_PREFIX = "osgi-dp:";

    // Injected by Felix DM...
    private volatile BundleContext m_context;

    @Override
    public void modifyConfiguration(ServiceReference<?> reference, Dictionary<String, Object> properties) {
        String bsn = reference.getBundle().getSymbolicName();

        String bundleLocation = null;
        String origin = null;
        String type = null;

        if (properties != null) {
            // Remove our custom properties from the configuration properties...
            bundleLocation = (String) properties.remove(CONFIGURATOR_RESOURCE_BUNDLE_LOCATION);
            origin = (String) properties.remove(CONFIGURATOR_RESOURCE_ORIGIN);
            type = (String) properties.remove(CONFIGURATOR_RESOURCE_TYPE);
        }

        if (!CONFIGURATOR_TYPE_AUTOCONF.equals(type)) {
            // We're done, this check only applies to AutoConf resources...
            return;
        }

        // Throwing an exception does not make much sense here (it is caught and logged only), so the only
        // option here is to terminate the VM in case we come across an fatal error...
        boolean exitOnFailure = !disableFatalDuplicateBundleCheck();

        if (bundleLocation != null && !"".equals(bundleLocation.trim())) {
            if (bundleLocation.startsWith(DP_PREFIX) && !bsn.equals(bundleLocation.substring(DP_PREFIX.length()))) {
                error("Bundle \"%s\" is not the same as \"%s\" for %s resource originating from \"%s\"!", bsn, bundleLocation, type, origin);

                if (exitOnFailure) {
                    // When using the AutoConf with DeploymentAdmin, an AutoConf resource only can
                    // refer to a bundle by its BSN (instead of its bundle location). This means that
                    // if the bundle location does *not* match the BSN of the bundle we're about to
                    // give this configuration to, the provisioning will fail.
                    System.exit(255);
                }
            }
        }

        long bundleCount = Arrays.stream(m_context.getBundles()).filter(b -> bsn.equals(b.getSymbolicName())).count();
        if (bundleCount > 1) {
            error("Bundle \"%s\" is installed multiple times with different versions! Cannot provision %s resource from \"%s\"!", bsn, type, origin);

            if (exitOnFailure) {
                // When using the AutoConf with DeploymentAdmin, a deployment package cannot contain
                // duplicate bundles. Provisioning a configuration in such situation causes it to fail...
                System.exit(255);
            }
        }
    }

    /**
     * @return <code>true</code> if the check for duplicate bundles should *not* be fatal, <code>false</code> if it
     *         should be considered fatal.
     */
    private boolean disableFatalDuplicateBundleCheck() {
        String prop = m_context.getProperty("org.amdatu.configurator.disableFatalDuplicateBundleCheck");
        return Boolean.TRUE.equals(Boolean.parseBoolean(prop));
    }
}
