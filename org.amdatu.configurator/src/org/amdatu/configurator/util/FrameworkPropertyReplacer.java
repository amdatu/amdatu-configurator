/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

import org.amdatu.configurator.util.StringReplacementInputStream.Replacer;
import org.osgi.framework.BundleContext;

/**
 * Provides a {@link Replacer} implementation that retrieves the properties from the framework as property.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class FrameworkPropertyReplacer implements Replacer {
    private final String m_prefix;
    private final BundleContext m_context;
    private final boolean m_replaceAllPlaceholders;

    /**
     * Creates a new {@link FrameworkPropertyReplacer} instance.
     */
    public FrameworkPropertyReplacer(BundleContext context, String prefix, boolean replaceAllPlaceholders) {
        m_context = context;
        m_prefix = (prefix == null) ? "" : prefix.trim();
        m_replaceAllPlaceholders = replaceAllPlaceholders;
    }

    @Override
    public String replace(String input) {
        if (input == null) {
            return null;
        }
        if (input.startsWith(m_prefix)) {
            input = input.substring(m_prefix.length());
        }
        String value = m_context.getProperty(input);
        // AMDATUCONF-7: fall back to environment variables in case no framework & system property is found...
        if (value == null) {
            value = System.getenv(input);
        }
        if (value == null && m_replaceAllPlaceholders) {
            // Consume all unknown values and do replace the placeholder...
            return "";
        }
        return value;
    }
}
