/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.configurationbuilder;

import java.util.function.Function;

/**
 * A configuration, this represents a (factory) configuration that will be registered with the
 * {@link org.osgi.service.cm.ConfigurationAdmin} service.
 *
 * @param <C> the configuration type for this configuration
 */
public interface Configuration<C> {

	/**
	 * Set a property
	 *
	 * @param propertyKey Property key
	 * @return A {@link ConfigurationValueReceiver}
	 */
	ConfigurationValueReceiver<Object, C> set(String propertyKey);

	/**
	 * Set a property, this method takes a Function the name of the last method that's called on the configuration type
	 * is used to determine the property key. The return type of the function will be used to create
	 * {@link ConfigurationValueReceiver} that takes the property value in a type safe manner.
	 *
	 * NOTE: The configuration type instance that is passed to the propertyKeyRecordFunction is a proxy object that will
	 * always return the return null or the primitive type's default value when the return type is a primitive.
	 *
	 * @param propertyKeyRecordFunction Function used to get the property name based on
	 * @return A {@link ConfigurationValueReceiver}
	 * @throws IllegalArgumentException When no method has been called on the configuration type
	 */
	<X> ConfigurationValueReceiver<X, C> set(Function<C, X> propertyKeyRecordFunction) throws IllegalArgumentException;

	/**
	 * Add the configuration to the {@link ConfigurationBuilder}.
	 * @return The {@link ConfigurationBuilder}
	 */
	ConfigurationBuilder add();
}
