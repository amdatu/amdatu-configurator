/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_FILE_EXTENSIONS;
import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_TYPE;
import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_TYPE_AUTOCONF;
import static org.amdatu.configurator.autoconf.AutoConfigurator.SUPPORTED_FILE_EXTS;

import java.util.Properties;

import org.amdatu.configurator.Configurator;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationPlugin;
import org.osgi.service.cm.SynchronousConfigurationListener;
import org.osgi.service.metatype.MetaTypeService;

/**
 * Bundle Activator.
 */
public class Activator extends DependencyActivatorBase {
	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(CONFIGURATOR_TYPE, CONFIGURATOR_TYPE_AUTOCONF);
        props.put(CONFIGURATOR_FILE_EXTENSIONS, SUPPORTED_FILE_EXTS);
        
        String[] ifaces = { Configurator.class.getName(), SynchronousConfigurationListener.class.getName() };

		manager.add(createComponent()
		    .setInterface(ifaces, props)
			.setImplementation(AutoConfigurator.class)
			.add(createServiceDependency().setService(ConfigurationAdmin.class).setRequired(true))
			.add(createServiceDependency().setService(MetaTypeService.class).setRequired(false))
		);

		props = new Properties();
		props.put("service.cmRanking", Integer.valueOf(10));

		manager.add(createComponent()
	        .setInterface(ConfigurationPlugin.class.getName(), props)
	        .setImplementation(AutoConfConfigurationChecker.class)
	    );
	}
}
