/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import org.amdatu.configurator.util.StringReplacementInputStream.Replacer;
import org.junit.Test;

/**
 * Test cases for {@link StringReplacementInputStream}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class StringReplacementInputStreamTest {
    /* By default supported on any JVM. */
    private static final String UTF_8 = "UTF-8";

    @Test
    public void testHandleEmptyStreamOk() throws Exception {
        InputStream is = createInputStream("", createUppercaseReplacer());
        assertReplacement(is, "");
    }

    @Test
    public void testHandleNonUnicodeStreamWithoutPlaceholderOk() throws Exception {
        final String charName = "iso8859-15";
        final byte[] bytes = { (byte) 104, (byte) 164, (byte) 108, (byte) 108, (byte) 189 };

        InputStream is = createInputStream(bytes, createUppercaseReplacer(), charName);
        assertReplacement(is, "h\u20ACll\u0153", charName);
    }

    @Test
    public void testHandleNonUnicodeStreamWithPlaceholderOk() throws Exception {
        final String charName = "iso8859-15";
        final byte[] bytes = { '$', '{', (byte) 104, (byte) 164, (byte) 108, (byte) 108, (byte) 189, '}' };

        InputStream is = createInputStream(bytes, createUppercaseReplacer(), charName);
        assertReplacement(is, "H\u20ACLL\u0152", charName);
    }

    @Test
    public void testHandleStreamPartialPlaceholderOk() throws Exception {
        InputStream is = createInputStream("${one", createNumericReplacer());
        assertReplacement(is, "${one");
    }

    @Test
    public void testHandleStreamPartialReplacementOk() throws Exception {
        InputStream is = createInputStream("testing ${zero} ${one} ${two} ${three} ${four}!", createNumericReplacer());
        assertReplacement(is, "testing ${zero} 1 2 3 ${four}!");
    }

    /**
     * Additional test case for AMDATUCONF-3.
     */
    @Test
    public void testHandleStreamWithDollarBracketAtEndOk() throws Exception {
        InputStream is = createInputStream("hello ${", createUppercaseReplacer());
        assertReplacement(is, "hello ${");
    }

    @Test
    public void testHandleStreamWithEmbeddedPlaceholderOk() throws Exception {
        InputStream is = createInputStream("hello ${${name}}", createUppercaseReplacer());
        assertReplacement(is, "hello ${NAME}");
    }

    /**
     * Test case for AMDATUCONF-8. 
     */
    @Test
    public void testHandleStreamWithEmptyReplacementOk() throws Exception {
        InputStream is = createInputStream("hello '${empty}' '${single}'!", createEmptyReplacer());
        assertReplacement(is, "hello '' ' '!");
    }

    @Test
    public void testHandleStreamWithEmptyVarOk() throws Exception {
        InputStream is = createInputStream("hello '${}' '${ }'!", createUppercaseReplacer());
        assertReplacement(is, "hello '' ' '!");
    }

    @Test
    public void testHandleStreamWithLongReplacementsOk() throws Exception {
        InputStream is = createInputStream("hello ${0} ${1}!", new Replacer() {
            @Override
            public String replace(String input) {
                return "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";
            }
        });
        assertReplacement(
            is,
            "hello 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789!");
    }

    @Test
    public void testHandleStreamWithLongVarNamesOk() throws Exception {
        InputStream is;
        // Maximum var-name is 97 characters (100 - 3), when 98 characters are seen, do not replace anything...
        is = createInputStream("hello ${01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567}!", createUppercaseReplacer());
        assertReplacement(is, "hello ${01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567}!");
        // Maximum var-name is 97 characters (100 - 3), when 97 characters are seen, replace it...
        is = createInputStream("hello ${0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456}!", createUppercaseReplacer());
        assertReplacement(is, "hello 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456!");
        // Maximum var-name is 97 characters (100 - 3)...
        is = createInputStream("hello ${01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567", createUppercaseReplacer());
        assertReplacement(is, "hello ${01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567");
    }

    @Test
    public void testHandleStreamWithoutPlaceholderOk() throws Exception {
        InputStream is = createInputStream("hello world", createUppercaseReplacer());
        assertReplacement(is, "hello world");
    }

    @Test
    public void testHandleStreamWithoutReplacementOk() throws Exception {
        InputStream is = createInputStream("hello ${first} ${last}!", createNullReplacer());
        assertReplacement(is, "hello ${first} ${last}!");
    }

    /**
     * Additional test case for AMDATUCONF-3.
     */
    @Test
    public void testHandleStreamWithSingleDollarAtEndOk() throws Exception {
        InputStream is = createInputStream("hello $", createUppercaseReplacer());
        assertReplacement(is, "hello $");
    }

    /**
     * Additional test case for AMDATUCONF-3.
     */
    @Test
    public void testHandleStreamWithSingleDollarSignOk() throws Exception {
        InputStream is = createInputStream("hello $first ${last}!", createUppercaseReplacer());
        assertReplacement(is, "hello $first LAST!");
    }

    @Test
    public void testHandleStreamWithSinglePlaceholderOk() throws Exception {
        InputStream is = createInputStream("hello ${name}", createUppercaseReplacer());
        assertReplacement(is, "hello NAME");
    }

    @Test
    public void testHandleStreamWithTwoPlaceholderOk() throws Exception {
        InputStream is = createInputStream("hello ${first} ${last}!", createUppercaseReplacer());
        assertReplacement(is, "hello FIRST LAST!");
    }

    @Test
    public void testHandleStreamWithUnicodeCharsOk() throws Exception {
        InputStream is = createInputStream("Testing \u00AB\u03C4\u03B1\u0411\u042C\u2113\u03C3\u00BB: 1<2 & 4+1>3, now% off!", createUppercaseReplacer());
        assertReplacement(is, "Testing \u00AB\u03C4\u03B1\u0411\u042C\u2113\u03C3\u00BB: 1<2 & 4+1>3, now% off!");
    }

    @Test
    public void testHandleStreamWithUnicodeVarsOk() throws Exception {
        InputStream is = createInputStream("Testing \u00AB${var1}\u00BB: ${var2} ${\u00AB\u03C4\u03B1\u0411\u042C\u2113\u03C3\u00BB}!", new Replacer() {
            @Override
            public String replace(String input) {
                if ("var1".equals(input)) {
                    return "\u03C4\u03B1\u0411\u042C\u2113\u03C3";
                } else if ("var2".equals(input)) {
                    return "1<2 & 4+1>3, now% off";
                } else if ("\u00AB\u03C4\u03B1\u0411\u042C\u2113\u03C3\u00BB".equals(input)) {
                    return "text";
                }
                return null;
            }
        });
        assertReplacement(is, "Testing \u00AB\u03C4\u03B1\u0411\u042C\u2113\u03C3\u00BB: 1<2 & 4+1>3, now% off text!");
    }

    @Test
    public void testPushbackAndReadingOk() throws Exception {
        StringReplacementInputStream is = createInputStream("test", createNullReplacer());

        assertEquals('t', is.read());
        // push back some data that should be prepended for the current text...
        is.pushback(new byte[] { 'h', 'a', 'v', 'e', ' ', 'a', ' ', 'r' });

        byte[] buf = new byte[20];
        int read = is.read(buf, 0, buf.length);

        assertEquals(11, read);
        assertEquals("have a rest", new String(buf, 0, read));
    }

    private void assertReplacement(InputStream is, String expected) throws IOException {
        assertReplacement(is, expected, UTF_8);
    }

    private void assertReplacement(InputStream is, String expected, String charsetName) throws IOException {
        // See <weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html>
        try (Scanner scanner = new Scanner(is, charsetName)) {
            scanner.useDelimiter("\\A");

            if (scanner.hasNext()) {
                assertEquals(expected, scanner.next());
            }
            else {
                IOException ioException = scanner.ioException();
                if (ioException != null) {
                    throw ioException;
                }
                else {
                    assertEquals(expected, "");
                }
            }
        }
    }

    private Replacer createEmptyReplacer() {
        return new Replacer() {
            @Override
            public String replace(String input) {
                if ("empty".equals(input)) {
                    return "";
                } else if ("single".equals(input)) {
                    return " ";
                }
                return null;
            }
        };
    }

    private StringReplacementInputStream createInputStream(byte[] string, Replacer replacer, String charName) {
        return new StringReplacementInputStream(new ByteArrayInputStream(string), replacer, charName);
    }

    private StringReplacementInputStream createInputStream(String string, Replacer replacer) throws UnsupportedEncodingException {
        return createInputStream((string == null ? "" : string).getBytes(UTF_8), replacer, UTF_8);
    }

    private Replacer createNullReplacer() {
        return new Replacer() {
            @Override
            public String replace(String input) {
                return null;
            }
        };
    }

    private Replacer createNumericReplacer() {
        return new Replacer() {
            @Override
            public String replace(String input) {
                if ("one".equals(input)) {
                    return "1";
                }
                else if ("two".equals(input)) {
                    return "2";
                }
                else if ("three".equals(input)) {
                    return "3";
                }
                return null;
            }
        };
    }

    private Replacer createUppercaseReplacer() {
        return new Replacer() {
            @Override
            public String replace(String input) {
                if (input.matches("^[A-Z]$")) {
                    return input.toLowerCase();
                }
                return input.toUpperCase();
            }
        };
    }
}
