/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

/**
 * Constants used in Amdatu-Configurator.
 */
@ProviderType
public interface ConfiguratorConstants {
    /**
     * Represents a String service property that distinguishes the various registered {@link Configurator}s.
     * Implementations <b>must</b> provide this service property.
     */
    String CONFIGURATOR_TYPE = "type";

    /**
     * Represents a String-array service property that descripes what file extensions are supported by a
     * {@link Configurator} implementation. Implementations are encouraged to provide this service property.
     */
    String CONFIGURATOR_FILE_EXTENSIONS = "file.extensions";

    /**
     * Constant used for the AutoConf configurator.
     *
     * @see #CONFIGURATOR_TYPE
     */
    String CONFIGURATOR_TYPE_AUTOCONF = "autoconf";
    /**
     * Constant used for the properties-based configurator.
     *
     * @see #CONFIGURATOR_TYPE
     */
    String CONFIGURATOR_TYPE_PROPERTIES = "properties";

    /**
     * Property name used to provide the name of originating resource to the {@link ManagedService} or
     * {@link ManagedServiceFactory}.
     */
    String CONFIGURATOR_RESOURCE_ORIGIN = "amdatu.configurator.resource.origin";

    /**
     * Property name used to provide the bundle location as defined in the designate element of the
     * AutoConf resource. For property-based resources, this property should be <code>null</code>.
     */
    String CONFIGURATOR_RESOURCE_BUNDLE_LOCATION = "amdatu.configurator.resource.bundleLocation";

    /**
     * Property name used to provide the type of originating resource to the {@link ManagedService} or
     * {@link ManagedServiceFactory}, which should be the same as used for {@link #CONFIGURATOR_TYPE}.
     */
    String CONFIGURATOR_RESOURCE_TYPE = "amdatu.configurator.resource.type";
}
