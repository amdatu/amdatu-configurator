/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.amdatu.configurator.autoconf.MetaTypeUtil.listAutoConfResources;

import java.util.Iterator;
import java.util.List;

import org.apache.felix.metatype.MetaData;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfResources implements Iterable<AutoConfResource> {
    private final List<AutoConfResource> m_resources;

    /**
     * Creates a new {@link AutoConfResources} instance.
     */
    public AutoConfResources(String origin, MetaData metaData) {
        m_resources = listAutoConfResources(origin, metaData);
    }

    @Override
    public Iterator<AutoConfResource> iterator() {
        return m_resources.iterator();
    }

    /**
     * Verifies all AutoConf resources and checks them for common failures and errors.
     * 
     * @return <code>true</code> if the {@link AutoConfResource}s are valid and correct, <code>false</code> otherwise.
     */
    final boolean verify() {
        for (AutoConfResource resource : m_resources) {
            if (!resource.verify()) {
                return false;
            }
        }
        return true;
    }
}
