/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Provides an input stream that is capable of replacing parts of the content while it is being streamed.
 * <p>
 * This stream searches for a sequence of '${' and regards all characters until a '}' character is seen as being a
 * variable name. The maximum length of a variable name is 100 characters (including the '$', '{' and '}'). Variable
 * names are passed to a {@link Replacer}, which can provide a replacement value.
 * </p>
 * <p>
 * This class is <em>not</em> thread-safe!
 * </p>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class StringReplacementInputStream extends FilterInputStream {
    /** Extra room when reallocating buffers. */
    private static final int HEAD_ROOM = 10;
    /** Size of the buffer used for variables. */
    private static final int BUFFER_SIZE = 100;

    private static final byte DOLLAR = '$';
    private static final byte OPEN_BR = '{';
    private static final byte CLOSE_BR = '}';

    private final Replacer m_replacer;
    private final Charset m_charset;

    private byte[] m_buf;
    private int m_pos;

    /**
     * Creates a new {@link StringReplacementInputStream} instance for the <em>default</em> character set of the
     * running virtual machine.
     * 
     * @param in the input stream to replace any found variables in, cannot be <code>null</code>;
     * @param replacer the callback for obtaining replacements for variables, cannot be <code>null</code>.
     */
    public StringReplacementInputStream(InputStream in, Replacer replacer) {
        this(in, replacer, Charset.defaultCharset());
    }

    /**
     * Creates a new {@link StringReplacementInputStream} instance for a given {@link Charset} instance.
     * 
     * @param in the input stream to replace any found variables in, cannot be <code>null</code>;
     * @param replacer the callback for obtaining replacements for variables, cannot be <code>null</code>;
     * @param charset the character set to use for the input stream, cannot be <code>null</code>.
     */
    public StringReplacementInputStream(InputStream in, Replacer replacer, Charset charset) {
        super(in);

        m_buf = new byte[BUFFER_SIZE];
        m_pos = m_buf.length;

        m_replacer = replacer;
        m_charset = charset;
    }

    /**
     * Creates a new {@link StringReplacementInputStream} instance for a given character set name.
     * 
     * @param in the input stream to replace any found variables in, cannot be <code>null</code>;
     * @param replacer the callback for obtaining replacements for variables, cannot be <code>null</code>;
     * @param charset the name of character set to use for the input stream, cannot be <code>null</code>.
     */
    public StringReplacementInputStream(InputStream in, Replacer replacer, String charsetName) {
        this(in, replacer, Charset.forName(charsetName));
    }

    @Override
    public int available() throws IOException {
        ensureOpen();
        int n = m_buf.length - m_pos;
        int avail = super.available();
        return n > (Integer.MAX_VALUE - avail) ? Integer.MAX_VALUE : n + avail;
    }

    @Override
    public void close() throws IOException {
        if (in != null) {
            in.close();
            in = null;
        }
        m_buf = null;
    }

    @Override
    public void mark(int readlimit) {
        // Nop
    }

    @Override
    public boolean markSupported() {
        return false;
    }

    @Override
    public int read() throws IOException {
        ensureOpen();
        // Still in our pushback buffer?
        if (m_pos < m_buf.length) {
            return m_buf[m_pos++] & 0xff;
        }

        int read = super.read();
        if (read == DOLLAR) {
            int la = super.read();
            if (la == OPEN_BR) {
                // consume until we see a '}', or EOF...
                ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFFER_SIZE);
                int ch = 0, no = 0, max = BUFFER_SIZE - 3;
                do {
                    ch = super.read();
                    if (ch < 0 || ch == CLOSE_BR) {
                        break;
                    }
                    baos.write(ch);
                }
                while (++no <= max);
                // Check for an overflow in the var-name handling...
                boolean validName = (no <= max) && (ch == CLOSE_BR);

                String name = baos.toString(m_charset.name());
                if (validName) {
                    String replacement = m_replacer.replace(name);
                    if (replacement != null) {
                        // There's something to replace, but do this only if we have a non-empty string...
                        byte[] bytes = replacement.getBytes(m_charset);
                        if (bytes.length > 0) {
                            pushback(bytes, 1, bytes.length - 1);
                            return bytes[0];
                        }
                        else {
                            // AMDATUCONF-8: Empty replacement values are easy: just return the next character...
                            return super.read();
                        }
                    }
                }

                if (ch == CLOSE_BR) {
                    pushback(CLOSE_BR);
                }
                pushback(name.getBytes(m_charset));
                pushback(OPEN_BR);
            }
            else if (la >= 0) {
                // AMDATUCONF-3: only push back the just-read character, and return
                // the dollar (which is still in the read var)...
                pushback((byte) la);
            }
        }
        return read;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        ensureOpen();
        if (b == null) {
            throw new NullPointerException();
        }
        else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        }
        else if (len == 0) {
            return 0;
        }

        int avail = m_buf.length - m_pos;
        if (avail > 0) {
            if (len < avail) {
                avail = len;
            }
            System.arraycopy(m_buf, m_pos, b, off, avail);
            m_pos += avail;
            off += avail;
            len -= avail;
        }
        if (len > 0) {
            int c = read();
            if (c == -1) {
                return -1;
            }
            b[off] = (byte) c;

            int i = 1;
            for (; i < len; i++) {
                c = read();
                if (c == -1) {
                    break;
                }
                b[off + i] = (byte) c;
            }
            return avail + i;
        }
        return avail;
    }

    @Override
    public void reset() throws IOException {
        throw new IOException("mark/reset not supported");
    }

    @Override
    public long skip(long n) throws IOException {
        ensureOpen();
        if (n <= 0) {
            return 0;
        }

        long pskip = m_buf.length - m_pos;
        if (pskip > 0) {
            if (n < pskip) {
                pskip = n;
            }
            m_pos += pskip;
            n -= pskip;
        }
        if (n > 0) {
            pskip += super.skip(n);
        }
        return pskip;
    }

    /**
     * Check to make sure that this stream has not been closed
     */
    private void ensureOpen() throws IOException {
        if (in == null) {
            throw new IOException("Stream closed");
        }
    }

    protected void pushback(byte... chs) throws IOException {
        pushback(chs, 0, chs.length);
    }

    protected void pushback(byte[] chs, int off, int len) throws IOException {
        ensureOpen();

        if (len <= m_pos) {
            m_pos -= len;
            System.arraycopy(chs, off, m_buf, m_pos, len);
        }
        else {
            int diff = len - m_pos;
            byte[] newBuf = new byte[HEAD_ROOM + m_buf.length + diff];
            if (m_buf.length != m_pos) {
                // Take over any data that is already pushed back...
                System.arraycopy(m_buf, m_pos, newBuf, HEAD_ROOM + len, m_buf.length - m_pos);
            }
            System.arraycopy(chs, off, newBuf, HEAD_ROOM, len);
            //
            m_pos = HEAD_ROOM;
            m_buf = newBuf;
        }
    }

    /**
     * Represents a simple property replacer, which can replace a given placeholder name for another value.
     * 
     * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
     */
    public static interface Replacer {
        /**
         * Returns the value for the placeholder with the given name.
         * 
         * @param input the placeholder name to replace, can be <code>null</code>.
         * @return the replacement value, or <code>null</code> if no replacement is available.
         */
        String replace(String input);
    }
}
