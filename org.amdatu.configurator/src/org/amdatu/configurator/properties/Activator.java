/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.properties;

import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_FILE_EXTENSIONS;
import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_TYPE;
import static org.amdatu.configurator.properties.PropertyConfigurator.SUPPORTED_FILE_EXTS;

import java.util.Properties;

import org.amdatu.configurator.Configurator;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationAdmin;

/**
 * Bundle activator for Property configurator.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        Properties props = new Properties();
        props.put(CONFIGURATOR_TYPE, "property");
        props.put(CONFIGURATOR_FILE_EXTENSIONS, SUPPORTED_FILE_EXTS);

        manager.add(createComponent()
            .setInterface(Configurator.class.getName(), props)
            .setImplementation(PropertyConfigurator.class)
            .add(createServiceDependency().setService(ConfigurationAdmin.class).setRequired(true))
        );
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        // Nop
    }
}
