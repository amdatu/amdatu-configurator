/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.configurationbuilder.itest;

import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.osgi.service.cm.ConfigurationAdmin.SERVICE_FACTORYPID;

import java.io.IOException;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.amdatu.configurator.configurationbuilder.ConfigurationBuilder;
import org.amdatu.testing.configurator.TestConfigurator;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.osgi.framework.Constants;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.SynchronousConfigurationListener;

@RunWith(JUnit4.class)
public class ConfigurationBuilderTest {

	private Component component;

	public @interface Source {

		String PID = "my.source.pid";

		String sourceString();

		int sourceInt();

		String string_with_dots();
	}

	public @interface Target {

		String PID = "my.target.pid";

		String targetString();

		int targetInt();

		String string_with_dots();
	}

	public @interface SecondTarget {

		String PID = "my.second.target.pid";

		String[] secondTargetStrings();
	}


	@SuppressWarnings("unused" /* Injected by dependency manager */)
	private volatile DependencyManager dependencyManager;
	@SuppressWarnings("unused" /* Injected by dependency manager */)
	private volatile ConfigurationAdmin configurationAdmin;

	// helper class to wait for configuration to be known in ConfigurationAdmin
	private static class WaitForCA implements SynchronousConfigurationListener {

		private Map<String, CountDownLatch> latches = new HashMap<>();

		void prepare(String pid) {
			latches.put(pid, new CountDownLatch(1));
		}

		void await(String pid) throws InterruptedException {
			CountDownLatch latch = latches.get(pid);
			Objects.requireNonNull(latch, "Latch null, reset first?");
			boolean success = latch.await(10, TimeUnit.SECONDS);
			latches.remove(pid);
			if (!success) {
				fail("Did not get expected ConfigurationEvent for pid: " + pid);
			}
		}

		@Override
		public void configurationEvent(ConfigurationEvent event) {
			String pid = event.getFactoryPid() == null ? event.getPid() : event.getFactoryPid();

			CountDownLatch latch = latches.get(pid);

			if (latch != null) {
				latch.countDown();
			}
		}
	}

	private final WaitForCA waitForCA = new WaitForCA();

	@Before
	public void before() {
		TestConfigurator.configure(this)
				.add(createComponent()
						.setInterface(SynchronousConfigurationListener.class.getName(), null)
						.setImplementation(waitForCA))
				.add(createServiceDependency()
						.setService(ConfigurationAdmin.class)
						.setRequired(true))
				.apply();
	}

	@After
	public void after() throws IOException, InvalidSyntaxException, InterruptedException {
		if (component != null) {
			dependencyManager.remove(component);
		}

		ConfigurationAdmin ca = configurationAdmin;

		Configuration[] configurations1 = ca.listConfigurations(String.format("(%s=my.source.pid)", Constants.SERVICE_PID));
		if (configurations1 != null) {
			for (Configuration configuration : configurations1) {
				configuration.delete();
			}
		}

		String configsCreatedInTestFilter = String.format("(|(%s=my.*)(%s=my.))", Constants.SERVICE_PID, ConfigurationAdmin.SERVICE_FACTORYPID);
		Configuration[] configurations = ca.listConfigurations(configsCreatedInTestFilter);
		int retries = 0;
		while (configurations != null && retries < 5) {
			Thread.sleep(100);
			configurations = ca.listConfigurations(configsCreatedInTestFilter);
			retries++;
		}
		if (configurations != null) {
			List<String> collect = Arrays.stream(configurations).map(Configuration::getPid).collect(Collectors.toList());
			fail("Didn't reach a clean state " + collect);
		}

		TestConfigurator.cleanUp(this);
	}

	@Test
	public void create() throws IOException, InterruptedException {
		waitForCA.prepare(Target.PID);
		component = ConfigurationBuilder.create()
				.configuration(Target.class, Target.PID)
				.set(Target::targetString).value("test")
				.set(Target::targetInt).value(456)
				.add()
				.register(dependencyManager);

		waitForCA.await(Target.PID);

		Configuration configuration = configurationAdmin.getConfiguration(Target.PID, null);

		Dictionary<String, Object> configurationProperties = configuration.getProperties();
		assertEquals(3, configurationProperties.size());
		assertEquals("test", configurationProperties.get("targetString"));
		assertEquals(456, configurationProperties.get("targetInt"));
	}

	@Test
	public void createMultiple() throws IOException, InterruptedException {
		waitForCA.prepare(Target.PID);
		waitForCA.prepare(SecondTarget.PID);
		component = ConfigurationBuilder.create()
				.configuration(Target.class, Target.PID)
				.set(Target::targetString).value("test")
				.set(Target::targetInt).value(456)
				.add()
				.configuration(SecondTarget.class, SecondTarget.PID)
				.set(SecondTarget::secondTargetStrings).value(new String[]{"string", "array"})
				.add()
				.register(dependencyManager);

		waitForCA.await(Target.PID);


		Configuration configuration = configurationAdmin.getConfiguration(Target.PID, null);

		Dictionary<String, Object> configurationProperties = configuration.getProperties();
		assertEquals(3, configurationProperties.size());
		assertEquals("test", configurationProperties.get("targetString"));
		assertEquals(456, configurationProperties.get("targetInt"));

		waitForCA.await(SecondTarget.PID);

		configuration = configurationAdmin.getConfiguration(SecondTarget.PID, null);

		configurationProperties = configuration.getProperties();
		assertEquals(2, configurationProperties.size());
		assertArrayEquals(new String[] {"string", "array"}, (String[]) configurationProperties.get("secondTargetStrings"));

	}

	@Test
	public void adaptRequired() throws IOException, InterruptedException, InvalidSyntaxException {
		String configsCreatedInTestFilter = String.format("(|(%s=my.*)(%s=my.))", Constants.SERVICE_PID, ConfigurationAdmin.SERVICE_FACTORYPID);
		Configuration[] configurations = configurationAdmin.listConfigurations(configsCreatedInTestFilter);
		assertNull(configurations);
		component = ConfigurationBuilder.adapt(Source.class, Source.PID)
				.configuration(Target.class, Target.PID)
				.set(Target::targetString).value(Source::sourceString)
				.set(Target::targetInt).value(Source::sourceInt)
				.set(Target::string_with_dots).value(Source::string_with_dots)
				.add()
				.register(dependencyManager);

		Dictionary<String, Object> conf = new Hashtable<>();
		conf.put("sourceString", "test");
		conf.put("sourceInt", 123);
		conf.put("string.with.dots", "I like dots!");

		waitForCA.prepare(Target.PID);

		Configuration sourceConfig = configurationAdmin.getConfiguration(Source.PID, null);
		sourceConfig.update(conf);

		waitForCA.await(Target.PID);

		Configuration configuration = configurationAdmin.getConfiguration(Target.PID, null);
		Dictionary<String, Object> configurationProperties = configuration.getProperties();
		assertEquals(4, configurationProperties.size());
		assertEquals("test", configurationProperties.get("targetString"));
		assertEquals(123, configurationProperties.get("targetInt"));
		assertEquals("I like dots!", configurationProperties.get("string.with.dots"));

		waitForCA.prepare(Target.PID);
		sourceConfig.delete();
		waitForCA.await(Target.PID);

		Configuration[] object = configurationAdmin.listConfigurations(String.format("(%s=%s)", Constants.SERVICE_PID, Target.PID));
		assertNull(object);

		waitForCA.prepare(Target.PID);

		conf.put("sourceString", "updated");
		sourceConfig = configurationAdmin.getConfiguration(Source.PID, null);
		sourceConfig.update(conf);

		waitForCA.await(Target.PID);

		configuration = configurationAdmin.getConfiguration(Target.PID, null);
		assertNotNull(configuration);
		assertNotNull(configuration.getProperties());
		configurationProperties = configuration.getProperties();
		assertEquals(4, configurationProperties.size());
		assertEquals("updated", configurationProperties.get("targetString"));
		assertEquals(123, configurationProperties.get("targetInt"));
		assertEquals("I like dots!", configurationProperties.get("string.with.dots"));
	}

	@Test
	public void adaptOptional() throws IOException, InterruptedException {
		waitForCA.prepare(Target.PID);
		component = ConfigurationBuilder.adapt(Source.class, Source.PID, false)
				.configuration(Target.class, Target.PID)
				.set(Target::targetString).value(Source::sourceString)
				.set(Target::targetInt).value(Source::sourceInt)
				.set(Target::string_with_dots).value(Source::string_with_dots)
				.add()
				.register(dependencyManager);

		waitForCA.await(Target.PID);

		Configuration configuration = configurationAdmin.getConfiguration(Target.PID, null);
		Dictionary<String, Object> configurationProperties = configuration.getProperties();
		assertEquals(2, configurationProperties.size());
		assertEquals(0, configurationProperties.get("targetInt"));

		waitForCA.prepare(Target.PID);

		Dictionary<String, Object> conf = new Hashtable<>();
		conf.put("sourceString", "test");
		conf.put("sourceInt", 123);
		conf.put("string.with.dots", "I like dots!");
		configurationAdmin.getConfiguration(Source.PID, null).update(conf);

		waitForCA.await(Target.PID);

		configuration = configurationAdmin.getConfiguration(Target.PID, null);
		configurationProperties = configuration.getProperties();
		assertEquals(4, configurationProperties.size());
		assertEquals("test", configurationProperties.get("targetString"));
		assertEquals(123, configurationProperties.get("targetInt"));
		assertEquals("I like dots!", configurationProperties.get("string.with.dots"));

	}

	@Test
	public void adaptAndConfigureFactory() throws IOException, InterruptedException, InvalidSyntaxException {
		component = ConfigurationBuilder.adapt(Source.class, Source.PID)
				.factoryConfiguration(Target.class, Target.PID)
				.set(Target::targetString).value(Source::sourceString)
				.set(Target::targetInt).value(Source::sourceInt)
				.set(Target::string_with_dots).value(Source::string_with_dots)
				.add()
				.register(dependencyManager);

		Dictionary<String, Object> conf = new Hashtable<>();
		conf.put("sourceString", "test");
		conf.put("sourceInt", 123);
		conf.put("string.with.dots", "I like dots!");

		waitForCA.prepare(Target.PID);

		configurationAdmin.getConfiguration(Source.PID, null).update(conf);

		waitForCA.await(Target.PID);

		Configuration[] configurations = configurationAdmin.listConfigurations(String.format("(%s=%s)", SERVICE_FACTORYPID, Target.PID));

		assertNotNull(configurations);
		assertEquals(1, configurations.length);

		Configuration configuration = configurations[0];
		Dictionary<String, Object> configurationProperties = configuration.getProperties();
		assertEquals(5, configurationProperties.size());
		assertEquals("test", configurationProperties.get("targetString"));
		assertEquals(123, configurationProperties.get("targetInt"));
		assertEquals("I like dots!", configurationProperties.get("string.with.dots"));
	}

	@Test
	public void adaptRawType() throws IOException, InterruptedException {
		component = ConfigurationBuilder.adapt(Dictionary.class, Source.PID)
				.configuration(Target.class, Target.PID)
				.set(Target::targetString).value(d -> (String) d.get("sourceString"))
				.set(Target::targetInt).value(d -> (Integer) d.get("sourceInt"))
				.set(Target::string_with_dots).value(d -> (String) d.get("string.with.dots"))
				.add()
				.register(dependencyManager);

		Dictionary<String, Object> conf = new Hashtable<>();
		conf.put("sourceString", "test");
		conf.put("sourceInt", 123);
		conf.put("string.with.dots", "I like dots!");

		waitForCA.prepare(Target.PID);

		configurationAdmin.getConfiguration(Source.PID, null).update(conf);

		waitForCA.await(Target.PID);

		Configuration configuration = configurationAdmin.getConfiguration(Target.PID, null);
		Dictionary<String, Object> configurationProperties = configuration.getProperties();
		assertEquals(4, configurationProperties.size());
		assertEquals("test", configurationProperties.get("targetString"));
		assertEquals(123, configurationProperties.get("targetInt"));
		assertEquals("I like dots!", configurationProperties.get("string.with.dots"));
	}

	@Test
	public void configureRawType() throws IOException, InterruptedException {
		waitForCA.prepare(Target.PID);
		component = ConfigurationBuilder.create()
				.configuration(Dictionary.class, Target.PID)
				.set("targetString").value("test")
				.set("targetInt").value(456)
				.add()
				.register(dependencyManager);

		waitForCA.await(Target.PID);

		Configuration configuration = configurationAdmin.getConfiguration(Target.PID, null);

		Dictionary<String, Object> configurationProperties = configuration.getProperties();
		assertEquals(3, configurationProperties.size());
		assertEquals("test", configurationProperties.get("targetString"));
		assertEquals(456, configurationProperties.get("targetInt"));
	}

}
