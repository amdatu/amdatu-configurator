/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.configurationbuilder;

import org.amdatu.configurator.configurationbuilder.impl.ConfigurationBuilderImpl;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;

/**
 * Helper to create configurations programmatically and register them in the {@link org.osgi.service.cm.ConfigurationAdmin}
 * service.
 * <p>
 * Optionally the configuration that is created can adapt another configuration from the
 * {@link org.osgi.service.cm.ConfigurationAdmin} service.
 * <p>
 * Usage example:
 *
 * <pre>
 * ConfigurationBuilder.create()
 * 		.configuration(Target.class, Target.PID)
 * 		.set(Target::targetString).value("test")
 * 		.set(Target::targetInt).value(456)
 * 		.add()
 * 		.register(dependencyManager);
 *
 * </pre>
 */
public interface ConfigurationBuilder {

	/**
	 * Create a new {@link ConfigurationBuilder}
	 *
	 * @return a {@link ConfigurationBuilder} instance
	 */
	static ConfigurationBuilder create() {
		return new ConfigurationBuilderImpl<>();
	}

	/**
	 * Create a new {@link AdapterConfigurationBuilder} to create a configuration that adapts another configuration
	 * <p>
	 * Same as {@link ConfigurationBuilder#adapt(Class, String, boolean)} with <code>required</code> set to <code>true</code>
	 *
	 * @param sourceConfigClass the configuration type of the configuration to adapt
	 * @param pid               the pid of the configuration to adapt
	 * @return a {@link AdapterConfigurationBuilder} instance
	 */
	static <A> AdapterConfigurationBuilder<A> adapt(Class<A> sourceConfigClass, String pid) {
		return adapt(sourceConfigClass, pid, true);
	}

	/**
	 * Create a new {@link AdapterConfigurationBuilder} to create a configuration that adapts another configuration.
	 * <p>
	 * In case the adapted configuration is required the configuration(s) will only be registered when the adapted
	 * configuration is available registered with in the {@link org.osgi.service.cm.ConfigurationAdmin} service.
	 * When the adapted configuration is optional the configuration(s) will always be registered.
	 *
	 * @param sourceConfigClass the configuration type of the configuration to adapt
	 * @param pid               the pid of the configuration to adapt
	 * @param required          boolean indicating whether the adapted configuration is required or not
	 * @return a {@link AdapterConfigurationBuilder} instance
	 */
	static <A> AdapterConfigurationBuilder<A> adapt(Class<A> sourceConfigClass, String pid, boolean required) {
		return new ConfigurationBuilderImpl<>(sourceConfigClass, pid, required);
	}

	/**
	 * Create a new configuration
	 *
	 * @param configurationType the configuration type of the configuration to create
	 * @param pid                the pid of the configuration to create
	 * @return a {@link Configuration} instance
	 */
	<C> Configuration<C> configuration(Class<C> configurationType, String pid);

	/**
	 * Create a new facotry configuration
	 *
	 * @param configurationType the configuration type of the factory configuration to create
	 * @param factoryPid         the factory pid of the configuration to create
	 * @return a {@link Configuration} instance
	 */
	<C> Configuration<C> factoryConfiguration(Class<C> configurationType, String factoryPid);

	/**
	 * Register the configurations
	 *
	 * @param dependencyManager {@link DependencyManager} instance used to register the component handling registration
	 *                          of the configuration(s)
	 */
	Component register(DependencyManager dependencyManager);
}
