/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator;

import java.io.File;
import java.io.IOException;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

/**
 * Represents a service that can provision configurations to {@link org.osgi.service.cm.ConfigurationAdmin} using
 * file-based resources.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@ProviderType
public interface Configurator {
    /**
     * Returns all provisioned configuration files.
     * <p>
     * Implementations should return the service PIDs of the configurations they have provisioned to
     * {@link ConfigurationAdmin}, allowing you to determine where a configuration came from.
     * </p>
     *
     * @return an array of provisioned configurations, never <code>null</code>.
     */
    String[] listProvisioned();

    /**
     * Provisions the given {@link File} as configuration.
     *
     * @param file
     *            the {@link File} to provision as configuration, cannot be <code>null</code>.
     * @return <code>true</code> if the configuration was successfully
     *         provisioned, <code>false</code> otherwise. Note that a result of
     *         <code>true</code> is <b>no</b> indication that the configuration
     *         itself is accepted, it can still be rejected by the {@link ManagedService} or
     *         {@link ManagedServiceFactory}.
     * @throws IOException
     *             in case of I/O errors accessing the configuration file, or during the provisioning of the
     *             configuration.
     */
    boolean provision(File file) throws IOException;

    /**
     * Provisions all configuration files found in a given directory.
     * <p>
     * Implementations should try to install <em>all</em> found configuration files, continuing with the next file when
     * exceptions are thrown. Only the <em>first</em> exception should be rethrown after all resources have been
     * processed.
     * </p>
     * <p>
     * What files are considered as configuration files is considered an implementation detail. For example, it could
     * regard the name of the file as service PID, or obtain it by examining the file content.
     * </p>
     *
     * @param dir
     *            the directory to provision from, cannot be <code>null</code>.
     * @throws IOException
     *             in case of I/O errors accessing configuration files, or during the provisioning of the
     *             configurations.
     */
    void provisionAll(File dir) throws IOException;
}
