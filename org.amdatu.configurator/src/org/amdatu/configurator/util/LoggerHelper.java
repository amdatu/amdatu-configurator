/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

import static org.osgi.service.log.LogService.*;

import org.osgi.service.log.LogService;

/**
 * Provides a helper for logging stuff that happens during the provisioning of configurations.
 * <p>
 * It basically prints all log messages to the console, if enabled (by default it only logs warnings and errors). The
 * reason for not using something like {@link LogService} is that this bundle might be started <em>after</em> the Amdatu
 * Configurator bundles, causing our log-entries to get lost. Alternatively, we could store all log entries until a
 * {@link LogService} is present, but this introduces lots of complexity in the code for now.
 * </p>
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class LoggerHelper {
    /**
     * If set to a non-zero level, will cause log messages to be printed to the console (stdout). The integer value
     * specifies the log-level to use, where 1 = errors only, 2 = warnings and errors, 3 = info, warnings and errors and
     * 4 = all messages.
     */
    public static final String KEY_LOG_LEVEL = "org.amdatu.configurator.loglevel";
    
    private static final int DEFAULT_LOG_LEVEL = LOG_WARNING;

    private LoggerHelper() {
        // Nop
    }

    public static void debug(String msg, Object... args) {
        int level = getLogLevel();
        if (level < LOG_DEBUG) {
            return;
        }
        System.out.print("[DEBUG] ");
        System.out.printf(msg, args);
        System.out.println();
    }

    public static void info(String msg, Object... args) {
        int level = getLogLevel();
        if (level < LOG_INFO) {
            return;
        }
        System.out.print("[INFO] ");
        System.out.printf(msg, args);
        System.out.println();
    }

    public static void warn(String msg, Object... args) {
        int level = getLogLevel();
        if (level < LOG_WARNING) {
            return;
        }
        System.out.print("[WARN] ");
        System.out.printf(msg, args);
        System.out.println();
    }

    public static void error(String msg, Object... args) {
        int level = getLogLevel();
        if (level < LOG_ERROR) {
            return;
        }
        System.out.print("[ERROR] ");
        System.out.printf(msg, args);
        System.out.println();
    }

    private static int getLogLevel() {
        return Integer.getInteger(KEY_LOG_LEVEL, DEFAULT_LOG_LEVEL);
    }
}
