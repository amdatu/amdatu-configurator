/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.configurationbuilder.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.amdatu.configurator.configurationbuilder.AdapterConfiguration;
import org.amdatu.configurator.configurationbuilder.AdapterConfigurationBuilder;
import org.amdatu.configurator.configurationbuilder.AdapterConfigurationValueReceiver;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationBuilderImpl<A> implements AdapterConfigurationBuilder<A> {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationBuilderImpl.class);

	private final Class<A> adapteeConfigType;
	private final String adapteePid;
	private final boolean adapteeRequired;
	private final List<ConfigurationImpl<?>> configurations = new ArrayList<>();
	private final Map<ConfigurationImpl<?>, Configuration> caConfigMap = new HashMap<>();

	@SuppressWarnings("unused" /* injected by dependency manager */)
	private volatile ConfigurationAdmin configurationAdmin;

	// The configuration that's adapted set from updated method
	private volatile A adaptee;

	public ConfigurationBuilderImpl() {
		this(null, null, true);
	}

	public ConfigurationBuilderImpl(Class<A> adapteeConfigType, String adapteePid, boolean adapteeRequired) {
		this.adapteeConfigType = adapteeConfigType;
		this.adapteePid = adapteePid;
		this.adapteeRequired = adapteeRequired;
	}

	public class ConfigurationImpl<C> implements AdapterConfiguration<C, A> {
		private final String pid;
		private final boolean factory;
		private final PropertyKeyRecorder<C> propertyKeyRecorder;
		private final Map<String, Function<A, Object>> properties = new LinkedHashMap<>();

		ConfigurationImpl(Class<C> configType, String pid, boolean factory) {
			this.pid = pid;
			this.factory = factory;
			if (Dictionary.class.isAssignableFrom(configType)) {
				this.propertyKeyRecorder = null;
			} else {
				this.propertyKeyRecorder = new PropertyKeyRecorder<>(configType);
			}
		}

		@Override
		public AdapterConfigurationValueReceiver<Object, C, A> set(String propertyKey) {
			return new ValueReceiverImpl<>(propertyKey);
		}

		@Override
		public <X> AdapterConfigurationValueReceiver<X, C, A> set(Function<C, X> propertyKeyRecordFunction) {
			if (propertyKeyRecorder == null) {
				throw new IllegalStateException("Configuration#set(Function) is not supported when using configType Dictionary");
			}
			propertyKeyRecorder.clear();

			propertyKeyRecordFunction.apply(propertyKeyRecorder.record());
			String key = propertyKeyRecorder.getPropertyKey();

			if (key == null) {
				throw new IllegalArgumentException("Failed to get property key");
			}

			return new ValueReceiverImpl<>(key);
		}

		public class ValueReceiverImpl<X> implements AdapterConfigurationValueReceiver<X, C, A> {

			private final String propertyKey;

			ValueReceiverImpl(String propertyKey) {
				this.propertyKey = propertyKey;
			}

			@Override
			public AdapterConfiguration<C, A> value(X value) {
				properties.put(propertyKey, (s) -> value);
				return ConfigurationImpl.this;
			}

			@Override
			public AdapterConfiguration<C, A> value(Function<A, X> valueFunction) {
				return value(valueFunction, null);
			}

			@Override
			public AdapterConfiguration<C, A> value(Function<A, X> valueFunction, X defaultValue) {
				Function<A, X> f = s -> {
					X value = null;
					if (s != null) {
						value = valueFunction.apply(s);
					}
					return value != null ? value : defaultValue;
				};

				properties.put(propertyKey, f::apply);
				return ConfigurationImpl.this;
			}
		}

		@Override
		public AdapterConfigurationBuilder<A> add() {
			configurations.add(this);
			return ConfigurationBuilderImpl.this;
		}

		Dictionary<String, Object> getConfigurationDictionary(A adaptee) {

			Dictionary<String, Object> dictionary = new Hashtable<>();
			for (Map.Entry<String, Function<A, Object>> entry : properties.entrySet()) {
				Object value = entry.getValue().apply(adaptee);
				if (value != null) {
					dictionary.put(entry.getKey(), value);
				}
			}
			return dictionary;
		}

	}

	@Override
	public <C> ConfigurationImpl<C> configuration(Class<C> configurationType, String pid) {
		return new ConfigurationImpl<>(configurationType, pid, false);
	}

	@Override
	public <C> ConfigurationImpl<C> factoryConfiguration(Class<C> configurationType, String pid) {
		return new ConfigurationImpl<>(configurationType, pid, true);
	}

	@Override
	public Component register(DependencyManager dependencyManager) {
		Component component = dependencyManager.createComponent()
				.setImplementation(this)
				.add(dependencyManager.createServiceDependency()
						.setService(ConfigurationAdmin.class)
						.setRequired(true));

		if (adapteePid != null) {
			component.add(dependencyManager.createConfigurationDependency()
					.setPid(adapteePid)
					.setRequired(adapteeRequired)
					.setCallback("updated")
			);
		}

		dependencyManager.add(component);
		return component;
	}

	@SuppressWarnings("unused" /* called by dependency manager*/)
	protected final void start() {
		try {
			synchronized (caConfigMap) {
				for (ConfigurationImpl<?> configuration : configurations) {
					Configuration caConfig;
					if (configuration.factory) {
						caConfig = configurationAdmin.createFactoryConfiguration(configuration.pid, null);
					}
					else {
						caConfig = configurationAdmin.getConfiguration(configuration.pid, null);
					}

					caConfig.update(configuration.getConfigurationDictionary(adaptee));
					caConfigMap.put(configuration, caConfig);
				}
			}
		}
		catch (RuntimeException | IOException e) {
			LOG.error("Failed to create configurations in ConfigurationAdmin", e);

			Map<ConfigurationImpl, Configuration> configurationMap;
			synchronized (caConfigMap) {
				configurationMap = new HashMap<>(this.caConfigMap);
				caConfigMap.clear();
			}

			for (Configuration caConfig : configurationMap.values()) {
				try {
					caConfig.delete();
				}
				catch (RuntimeException | IOException e1) {
					LOG.error("Failed to cleanup, delete configuration from ConfigurationAdmin failed", e);
				}
			}
		}
	}

	@SuppressWarnings("unused" /* called by dependency manager*/)
	protected final void stop() {
		Map<ConfigurationImpl, Configuration> configurationMap;
		synchronized (caConfigMap) {
			configurationMap = new HashMap<>(this.caConfigMap);
			this.caConfigMap.clear();
		}

		for (Configuration configuration : configurationMap.values()) {
			try {
				configuration.delete();
			}
			catch (RuntimeException | IOException e) {
				LOG.error("Failed delete configuration from ConfigurationAdmin", e);
			}
		}
	}

	@SuppressWarnings("unused" /* called by dependency manager*/)
	protected final void updated(Dictionary<String, Object> properties) {
		if (Dictionary.class.isAssignableFrom(adapteeConfigType)) {
			adaptee = adapteeConfigType.cast(properties);
		}
		else {
			if (properties != null) {
				adaptee = Configurable.create(adapteeConfigType, properties, new Hashtable<>());
			}
			else {
				adaptee = null;
			}
		}

		if (configurationAdmin == null) {
			return; // ConfigurationAdmin not available can't update
		}

		if (adaptee == null && adapteeRequired) {
			return; // No need to update we will be stopped
		}


		Map<ConfigurationImpl<?>, Configuration> configurationMap;
		synchronized (caConfigMap) {
			configurationMap = new HashMap<>(caConfigMap);
		}

		try {
			for (Map.Entry<ConfigurationImpl<?>, Configuration> configurationEntry : configurationMap.entrySet()) {
				ConfigurationImpl<?> configuration = configurationEntry.getKey();
				Configuration caConfiguration = configurationEntry.getValue();
				caConfiguration.update(configuration.getConfigurationDictionary(adaptee));
			}
		}
		catch (RuntimeException | IOException e) {
			LOG.error("Failed to update configurations", e);
		}

	}

}
