/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.properties;

import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_RESOURCE_ORIGIN;
import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_RESOURCE_TYPE;
import static org.amdatu.configurator.ConfiguratorConstants.CONFIGURATOR_TYPE_PROPERTIES;

import java.io.File;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Properties;

import org.amdatu.configurator.util.Resource;

/**
 * Represents a Property-file resource.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class PropertyFileResource implements Resource {
    private final String m_factoryPid;
    private final String m_pid;
    private final Dictionary<String, Object> m_config;

    /**
     * Creates a new {@link PropertyFileResource} instance.
     */
    public PropertyFileResource(File file, Properties props) {
        String name = stripFileExtension(file.getName());

        int idx = name.indexOf('-');
        if (idx >= 0) {
            // Service Factory PID...
            m_factoryPid = name.substring(0, idx);
            m_pid = name.substring(idx + 1);
        }
        else {
            // Service PID...
            m_factoryPid = null;
            m_pid = name;
        }

        Dictionary<String, Object> dict = new Hashtable<>(props.size());
        for (Entry<Object, Object> entry : props.entrySet()) {
            dict.put((String) entry.getKey(), entry.getValue());
        }
        
        // Add some additional information to get a clue where a configuration originates from...
        dict.put(CONFIGURATOR_RESOURCE_TYPE, CONFIGURATOR_TYPE_PROPERTIES);
        dict.put(CONFIGURATOR_RESOURCE_ORIGIN, file.getName());

        m_config = dict;
    }

    static boolean isEmtpy(String str) {
        return (str == null) || "".equals(str.trim());
    }

    static String stripFileExtension(String str) {
        if (isEmtpy(str)) {
            return "";
        }
        int idx = str.lastIndexOf('.');
        if (idx >= 0) {
            String part = str.substring(idx + 1);
            if ("cnf".equals(part) || "cfg".equals(part) || "config".equals(part) || "props".equals(part) || "properties".equals(part)) {
                return str.substring(0, idx);
            }
        }
        return str;
    }

    /**
     * @return the actual configuration properties, never <code>null</code>.
     */
    public Dictionary<String, ?> getConfiguration() {
        return m_config;
    }

    public String getFactoryPid() {
        return m_factoryPid;
    }

    /**
     * @return a "unique" identifier for this Property-file resource resource, never <code>null</code>.
     */
    public String getId() {
        String id = getFactoryPid();
        if (isEmtpy(id)) {
            id = getPid();
        }
        return id;
    }

    public String getPid() {
        return m_pid;
    }

    /**
     * @return <code>true</code> if this Property-file resource represents a factory configuration, <code>false</code>
     *         if it represents a "singleton" configuration.
     */
    public boolean isFactoryConfig() {
        return !isEmtpy(m_factoryPid);
    }
}
