/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.configurationbuilder;

import java.util.function.Function;

/**
 * Specialized {@link Configuration} adapting another configuration
 *
 * @param <C> the configuration type for this configuration
 * @param <A> the configuration type for the configuration that's adapted
 */
public interface AdapterConfiguration<C, A> extends Configuration<C> {

	/**
	 * @see Configuration#set(String)
	 */
	AdapterConfigurationValueReceiver<Object, C, A> set(String propertyKey);

	/**
	 * @see Configuration#set(Function)
	 */
	<X> AdapterConfigurationValueReceiver<X, C, A> set(Function<C, X> propertyKeyRecordFunction);

	/**
	 * @see Configuration#add()
	 */
	AdapterConfigurationBuilder<A> add();
}
