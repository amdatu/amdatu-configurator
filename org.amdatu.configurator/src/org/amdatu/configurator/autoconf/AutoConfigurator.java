/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.amdatu.configurator.util.LoggerHelper.info;
import static org.amdatu.configurator.util.LoggerHelper.warn;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Dictionary;

import org.amdatu.configurator.Configurator;
import org.amdatu.configurator.util.FrameworkPropertyReplacer;
import org.amdatu.configurator.util.ResourceRepository;
import org.amdatu.configurator.util.StringReplacementInputStream;
import org.amdatu.configurator.util.StringReplacementInputStream.Replacer;
import org.apache.felix.dm.Component;
import org.apache.felix.metatype.Designate;
import org.apache.felix.metatype.MetaData;
import org.apache.felix.metatype.MetaDataReader;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.SynchronousConfigurationListener;

/**
 * Simple AutoConf implementation.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfigurator implements Configurator, SynchronousConfigurationListener {
    public static final String KEY_CONFIG_DIR = "org.amdatu.configurator.autoconf.dir";
    public static final String KEY_VERBOSE = "org.amdatu.configurator.autoconf.verbose";
    public static final String KEY_PLACEHOLDER_PREFIX = "org.amdatu.configurator.autoconf.prefix";
    public static final String KEY_REPLACE_ALL_PLACEHOLDERS = "org.amdatu.configurator.autoconf.replaceAllPlaceholders";

    /** Default directory to use in case no directory is specified. */
    public static final String DEFAULT_CONFIG_DIR = "conf";
    /** Prefix used for placeholders in ACE. */
    public static final String DEFAULT_PLACEHOLDER_PREFIX = "context.";
    /** The file extensions this implementation looks for. */
    public static final String[] SUPPORTED_FILE_EXTS = { ".xml" };
    /** the file name used to store the information on what resources are provisioned. */
    private static final String AUTOCONF_REPO_FILENAME = "autoconf.repo";

    // Injected by Felix DM...
    private volatile BundleContext m_context;
    private volatile ConfigurationAdmin m_configAdmin;

    private final ResourceRepository<AutoConfResource> m_repository;

    /**
     * Creates a new {@link AutoConfigurator} instance.
     */
    public AutoConfigurator() {
        m_repository = new ResourceRepository<>();
    }

    @Override
    public String[] listProvisioned() {
        return m_repository.listPIDs();
    }

    @Override
    public boolean provision(File file) throws IOException {
        if (isVerbose()) {
            info("Installing configuration from \"%s\"...", file.getName());
        }

        Replacer replacer = new FrameworkPropertyReplacer(m_context, getPlaceholderPrefix(), isReplaceAllPlaceholders());
        try (InputStream fis = Files.newInputStream(file.toPath()); InputStream is = new StringReplacementInputStream(fis, replacer)) {
            // Read the entry as MetaType file...
            MetaData data = new MetaDataReader().parse(is);

            if (data == null) {
                warn("Not an AutoConf resource(s): \"%s\", refusing to process it...", file.getName());
                return false;
            }

            AutoConfResources resources = new AutoConfResources(file.getName(), data);
            if (resources.verify()) {
                for (AutoConfResource resource : resources) {
                    if (isVerbose()) {
                        info("Processing AutoConf resource \"%s\"...", resource.getId());
                    }
                    provision(resource);
                }
            }
            else {
                warn("Invalid AutoConf resource(s): \"%s\", refusing to process it...", file.getName());

                return false;
            }
        }

        return true;
    }

    @Override
    public void provisionAll(File dir) throws IOException {
        Exception firstException = null;

        File[] files = listConfigurations(dir);
        for (File file : files) {
            try {
                provision(file);
            }
            catch (Exception exception) {
                if (firstException == null) {
                    firstException = exception;
                }
                warn("Failed to provision AutoConf resource from \"%s\"...", exception, file.getName());
            }
        }

        if (firstException instanceof RuntimeException) {
            throw (RuntimeException) firstException;
        }
        else if (firstException instanceof IOException) {
            throw (IOException) firstException;
        }
        else if (firstException != null) {
            throw new RuntimeException("Failed to provision AutoConf resource!", firstException);
        }
    }

    protected void provision(AutoConfResource resource) throws IOException {
        Dictionary<String, ?> props = resource.getConfiguration();
        Designate designate = resource.getDesignate();

        String pid = designate.getPid();
        String factoryPID = designate.getFactoryPid();
        String location = resource.getBundleLocation();

        Configuration configuration;
        if (resource.isFactoryConfig()) {
            String generatedPid = m_repository.getConfigurationPid(resource);
            if (generatedPid == null) {
                if (isVerbose()) {
                    info("Creating new configuration for \"%s\" (not found in repository)...", factoryPID);
                }
                // See OSGi compendium r4.2.0, section 114.4.1...
                configuration = m_configAdmin.createFactoryConfiguration(factoryPID, location);
            }
            else {
                if (isVerbose()) {
                    info("Obtaining configuration for \"%s\" (found in repository)...", generatedPid);
                }
                // See OSGi compendium r4.2.0, section 114.4.1...
                configuration = m_configAdmin.getConfiguration(generatedPid, location);
            }
        }
        else {
            if (isVerbose()) {
                info("Obtaining configuration for \"%s\" (maybe found in repository)...", pid);
            }
            // See OSGi compendium r4.2.0, section 114.4.1...
            configuration = m_configAdmin.getConfiguration(pid, location);
        }

        if (configuration != null) {
            if (m_repository.add(configuration, resource)) {
                info("Provisioned new AutoConf resource: %s", resource.getId());
            }
            configuration.update(props);
        }
    }

    /**
     * Called by Felix DM when starting this component.
     */
    protected void start(Component component) throws IOException {
        File configDir = getConfigDirectory();
        File repositoryFile = getRepositoryFile();
        // Load the earlier provisioned configurations...
        m_repository.load(repositoryFile);

        try {
            provisionAll(configDir);
        }
        finally {
            // Store the earlier provisioned configurations...
            m_repository.store(repositoryFile);
        }
    }

    /**
     * Called by Felix DM when stopping this component.
     */
    protected void stop(Component component) throws IOException {
        File repositoryFile = getRepositoryFile();
        // Store the earlier provisioned configurations (just to be sure)...
        m_repository.store(repositoryFile);
    }

    private File getConfigDirectory() {
        String dir = m_context.getProperty(KEY_CONFIG_DIR);
        if (dir == null) {
            dir = DEFAULT_CONFIG_DIR;
        }
        return new File(dir);
    }

    private String getPlaceholderPrefix() {
        String prefix = m_context.getProperty(KEY_PLACEHOLDER_PREFIX);
        if (prefix == null) {
            prefix = DEFAULT_PLACEHOLDER_PREFIX;
        }
        return prefix;
    }

    private File getRepositoryFile() throws IOException {
        File dataArea = m_context.getDataFile("");
        if (dataArea == null) {
            throw new IOException("No peristent storage supported...");
        }
        return new File(dataArea, AUTOCONF_REPO_FILENAME);
    }

    /**
     * @return <code>true</code> if all placeholders should be replaced, even if no property is defined for it.
     */
    private boolean isReplaceAllPlaceholders() {
        return Boolean.parseBoolean(m_context.getProperty(KEY_REPLACE_ALL_PLACEHOLDERS));
    }

    private boolean isVerbose() {
        return Boolean.parseBoolean(m_context.getProperty(KEY_VERBOSE));
    }

    private File[] listConfigurations(File dir) {
        if (!dir.exists()) {
            warn("AutoConf configuration directory \"%s\" does not exist!", dir);
            return new File[0];
        }
        return dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                for (String ext : SUPPORTED_FILE_EXTS) {
                    if (name.endsWith(ext)) {
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void configurationEvent(ConfigurationEvent event) {
        int type = event.getType();
        switch (type) {
            case ConfigurationEvent.CM_DELETED:
                m_repository.remove(event.getPid());
                break;

            case ConfigurationEvent.CM_LOCATION_CHANGED:
            case ConfigurationEvent.CM_UPDATED:
                break;

            default:
                throw new RuntimeException("Unknown configuration event type: #" + type);
        }
    }
}
