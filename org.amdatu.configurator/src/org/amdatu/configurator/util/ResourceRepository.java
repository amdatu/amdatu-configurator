/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

import static org.amdatu.configurator.util.LoggerHelper.debug;
import static org.amdatu.configurator.util.LoggerHelper.warn;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.osgi.service.cm.Configuration;

/**
 * Keeps track of what configuration is provisioned by which {@link Resource}.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ResourceRepository<RESOURCE extends Resource> {
    private final ConcurrentMap<String, String> m_mapping;

    /**
     * Creates a new {@link ResourceRepository} instance.
     */
    public ResourceRepository() {
        m_mapping = new ConcurrentHashMap<>();
    }

    /**
     * Adds a mapping for a provisioned {@link Configuration} to a resource.
     *
     * @param configuration the provisioned {@link Configuration} to map, cannot be <code>null</code>;
     * @param resource the resource that maps to the given configuration, cannot be <code>null</code>.
     * @return <code>true</code> if the mapping was added (did not exist yet), <code>false</code> otherwise.
     */
    public boolean add(Configuration configuration, RESOURCE resource) {
        String id = createIdentity(resource);
        String pid = configuration.getPid();
        return m_mapping.putIfAbsent(pid, id) == null;
    }

    /**
     * Returns the PID of the configuration provisioned for the given resource.
     *
     * @param resource the resource to return the configuration PID for, cannot be <code>null</code>.
     * @return the configuration PID as string, or <code>null</code> if no configuration was mapped to the given
     *         resource.
     */
    public String getConfigurationPid(RESOURCE resource) {
        String id = createIdentity(resource);
        for (Map.Entry<String, String> entry : m_mapping.entrySet()) {
            if (id.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * @return <code>true</code> if this repository is empty, <code>false</code> otherwise.
     */
    public boolean isEmpty() {
        return m_mapping.isEmpty();
    }

    /**
     * @return an array with all service PIDs that are in this repository, never <code>null</code>.
     */
    public String[] listPIDs() {
        String[] result = new String[m_mapping.size()];
        return m_mapping.keySet().toArray(result);
    }

    /**
     * Loads the contents of this repository from a given {@link File}.
     *
     * @param repoFile the repository file to load the data from, cannot be <code>null</code>.
     * @throws IOException in case of I/O problems reading from the repository file.
     */
    @SuppressWarnings("unchecked")
	public void load(File repoFile) throws IOException {
        try (InputStream fis = Files.newInputStream(repoFile.toPath()); ObjectInputStream ois = new ObjectInputStream(fis)) {
            m_mapping.putAll((Map<String, String>) ois.readObject());
            debug("Repository (%s) loaded...", repoFile.getName());
        }
        catch (NoSuchFileException | EOFException exception) {
            // Ignore, first time start...
            debug("Ignoring exception that is probably due to first use of: %s", repoFile);
        }
        catch (ClassNotFoundException | StreamCorruptedException exception) {
            warn("Invalid repository data while trying to load: %s", repoFile);
            throw new IOException("Invalid repository data?!", exception);
        }
    }

    /**
     * Removes a mapping for a provisioned {@link Configuration} to a resource.
     *
     * @param pid the PID of the resource to remove, cannot be <code>null</code>.
     * @return <code>true</code> if the mapping was removed (did exist), <code>false</code> otherwise.
     */
    public boolean remove(String pid) {
        return m_mapping.remove(pid) != null;
    }

    /**
     * Stores the contents of this repository to a given {@link File}.
     *
     * @param repoFile the repository file to store the data to, cannot be <code>null</code>.
     * @throws IOException in acse of I/O problems writing to the repository file.
     */
    public void store(File repoFile) throws IOException {
        try (OutputStream fos = Files.newOutputStream(repoFile.toPath()); ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(m_mapping);
            debug("Repository (%s) stored...", repoFile.getName());
        }
    }

    private String createIdentity(RESOURCE resource) {
        String factoryPid = resource.getFactoryPid();
        if (factoryPid != null) {
            return String.format("%s-%s", factoryPid, resource.getPid());
        }
        return resource.getPid();
    }
}
