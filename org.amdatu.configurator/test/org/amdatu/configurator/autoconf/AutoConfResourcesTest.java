/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;

import org.apache.felix.metatype.MetaData;
import org.apache.felix.metatype.MetaDataReader;
import org.junit.*;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfResourcesTest {

    @Test
    public void testDesignateWithBundleLocationPrefixOnlyFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("designateWithBundleLocationPrefixOnly.xml");
        assertFalse("Designate with invalid bundle location considered valid?", resourcesValid(acs));
    }

    @Test
    public void testDesignateWithEmptyBundleLocationFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("designateWithEmptyBundleLocation.xml");
        assertFalse("Designate without bundle location considered valid?", resourcesValid(acs));
    }

    @Test
    public void testDesignateWithMergeAttributeFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mergeDesignate.xml");
        assertFalse("Designate with merge attribute set considered valid?", resourcesValid(acs));
    }

    @Test
    public void testDesignateWithNonDPBundleLocationFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("designateWithNonDPBundleLocation.xml");
        assertFalse("Designate without osgi-dp prefix as bundle location considered valid?", resourcesValid(acs));
    }

    @Test
    public void testDesignateWithoutBundleLocationFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("designateWithoutBundleLocation.xml");
        assertFalse("Designate without bundle location considered valid?", resourcesValid(acs));
    }

    /**
     * Designate needs at least one object.
     */
    @Ignore //AMDATUCONF-14: Due to FELIX-4973, the rules for metatype parsing are a bit more relaxed...
    @Test(expected = IOException.class)
    public void testDesignateWithoutObjectFail() throws Exception {
        assertNull(createAutoConfResources("designateWithoutObject.xml"));
    }

    @Test
    public void testDesignateWithoutOcdOk() throws Exception {
        AutoConfResources acs = createAutoConfResources("designateWithoutOCD.xml");
        assertTrue("Designate without OCD not considered valid?", resourcesValid(acs));
    }

    @Test
    public void testDesignateWithStarBundleLocationFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("designateWithStarBundleLocation.xml");
        assertFalse("Designate without star bundle location considered valid?", resourcesValid(acs));
    }

    /**
     * An Designate needs at most one Object.
     */
    @Test(expected = IOException.class)
    public void testDesignateWithTwoObjectsFail() throws Exception {
        assertNull(createAutoConfResources("designateWithTwoObjects.xml"));
    }

    @Test
    public void testMandatoryADwithAttributeOk() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithAttribute.xml");
        assertTrue("Required attribute is NOT missing, but detected as such?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithCardinalityMinThreeMultipleCharacterValuesFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithWrongValue6.xml");
        assertFalse("Required attribute with two given values is NOT detected as such?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithCardinalityTwoMultipleCharacterValuesFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithWrongValue5.xml");
        assertFalse("Required attribute with two given values is NOT detected as such?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithDefaultFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithDefault.xml");
        assertFalse("Mandatory attribute with default considered valid?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithEmptyValueFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithWrongValue1.xml");
        assertFalse("Required attribute with empty value is NOT detected as such?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithInvalidCharacterValueFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithWrongValue3.xml");
        assertFalse("Required attribute with invalid value is NOT detected as such?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithInvalidIntegerValueFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithWrongValue2.xml");
        assertFalse("Required attribute with invalid value is NOT detected as such?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithoutAttributeFails() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithoutAttribute.xml");
        assertFalse("Required attribute is missing, but not detected?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithoutDefaultFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithoutDefault.xml");
        assertFalse("Mandatory attribute with default considered valid?", resourcesValid(acs));
    }

    @Test
    public void testMandatoryADwithSingleCardinalityMultipleCharacterValuesFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("mandatoryADwithWrongValue4.xml");
        assertFalse("Required attribute with two given values is NOT detected as such?", resourcesValid(acs));
    }

    @Test
    public void testObjectWithDuplicateAttributeFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("objectWithDuplicateAttribute.xml");
        assertFalse("Object with duplicate attribute considered valid?", resourcesValid(acs));
    }

    @Test
    public void testObjectWithoutOcdRefFail() throws Exception {
        AutoConfResources acs = createAutoConfResources("objectWithoutOcdRef.xml");
        assertFalse("Object without ocdref attribute considered valid?", resourcesValid(acs));
    }

    /**
     * An OCD needs at least one AD.
     */
    @Ignore //AMDATUCONF-14: Due to FELIX-4973, the rules for metatype parsing are a bit more relaxed...
    @Test(expected = IOException.class)
    public void testOcdWithoutAdFail() throws Exception {
        assertNull(createAutoConfResources("ocdWithoutAD.xml"));
    }

    @Test
    public void testOptionalADwithAttributeOk() throws Exception {
        AutoConfResources acs = createAutoConfResources("optionalADwithAttribute.xml");
        assertTrue("Optional attribute is NOT missing, but detected as such?", resourcesValid(acs));
    }

    @Test
    public void testOptionalADWithDefaultButWithoutAttributeOk() throws Exception {
        AutoConfResources acs = createAutoConfResources("optionalADwithDefaultButWithoutAttribute.xml");
        assertTrue("Optional attribute is missing, but detected as such?", resourcesValid(acs));
    }

    @Test
    public void testOptionalADwithDefaultOk() throws Exception {
        AutoConfResources acs = createAutoConfResources("optionalADwithDefault.xml");
        assertTrue("Optional attribute is NOT missing, but detected as such?", resourcesValid(acs));
    }

    @Test
    public void testOptionalADwithEmptyValueOk() throws Exception {
        AutoConfResources acs = createAutoConfResources("optionalADwithEmptyAttribute.xml");
        assertTrue("Optional attribute with empty value is detected as such?", resourcesValid(acs));
    }

    @Test
    public void testOptionalADWithoutDefaultAndAttributeOk() throws Exception {
        AutoConfResources acs = createAutoConfResources("optionalADwithoutDefaultAndAttribute.xml");
        assertTrue("Optional attribute is missing, but detected as such?", resourcesValid(acs));
    }

    private AutoConfResources createAutoConfResources(String name) throws Exception {
        try (InputStream is = getClass().getResourceAsStream(name)) {
            // Read the entry as MetaType file...
            MetaData data = new MetaDataReader().parse(is);
            assertNotNull("Not an AutoConf resource: " + name, data);

            return new AutoConfResources(name, data);
        }
    }

    private boolean resourcesValid(AutoConfResources acs) {
        // We're *not* running in an OSGi context here, so disable the verification of the bundle location as it would
        // yield false negatives...
        return acs.verify();
    }
}
